class OpenProjectParser {
    constructor(db, fs){
        this.db = db;
        this.fs = fs;
    }

    toTextile(wiki) {
        this.fs.writeFileSync('.wiki-tmp.org', '* ' + wiki);
        const execSync = require('child_process').execSync;
        this.code = execSync('pandoc --from org --to textile -o .wiki-tmp.textile .wiki-tmp.org');
        return this.fs.readFileSync('.wiki-tmp.textile', 'utf8');       
    }

    extractTagValue(headers, tag){
        let removePrevious = headers.substring(headers.indexOf(tag));
        return removePrevious.substring(tag.length, removePrevious.indexOf('\n') ).trim();
    }
    
    createHeaders(headers) {
        return {
            wikiId: this.extractTagValue(headers, 'WIKIPAGE:'),
            project: this.extractTagValue(headers, 'PROJECT:'),
            version: this.extractTagValue(headers, 'VERSION:'),
            author: this.extractTagValue(headers, 'AUTHOR:')
        };
    }

    extractStoryId(story){
        return story.substring(story.lastIndexOf(":work_package_id: ") + 18,story.lastIndexOf("\n:story_points:"));
    }

    extractStoryPoints(story){
        return story.substring(story.lastIndexOf(":story_points: ") + 15,story.lastIndexOf("\n:type:"));
    }

    extractStoryStatus(story, statuses){
        return statuses.filter( (status) => {
            return status.name.trim() == story.split(/\s/)[0].replace(/-/g, ' ').trim();
        })[0].id;
    }

    extractStorySubject(story){
        return story.substring(story.split(/\s/)[0].trim().length, story.indexOf(" :")).trim();
    }

    extractStoryDescription(story) {
        return story.substring(story.lastIndexOf(":END:")+5).trim();
    }

    createStory(story, statuses, props) {       
        return {    
            id : this.extractTagValue(story,'work_package_id:'),
            statusId : this.extractStoryStatus(story,statuses),
            subject : this.extractStorySubject(story),
            description : this.extractStoryDescription(story),
            project: props.project,
            version: props.version,
            author: props.author,
            priority: 2, //Hardcoded Normal Priority
            storyPoints: this.extractStoryPoints(story)
        };
    }
    
    async parseOrgFile(orgFile) {
        let statuses = await this.db.queryStatuses();
        let orgElems = orgFile.split('\n* ');
        let orgStories = orgElems.slice(1);
        let orgWiki = orgStories.pop();
        let props = this.createHeaders(orgElems[0]);
        let stories = orgStories.map( (story) => {
            return this.createStory(story, statuses, props);  
        });
        return { stories: stories , wiki: {id: props.wikiId, text: this.toTextile(orgWiki) } };
    };

    updateOpenProjectDatabase(file) {
        this.parseOrgFile(this.fs.readFileSync(file, 'utf8'))
            .then( (sprint) => {
                let promises = this.db.updateWorkPackages(sprint.stories);
                promises.push(this.db.updateWiki(sprint.wiki));
                Promise.all(promises)
                    .then( (x) => {
                        console.log("Update Success!");
                        process.exit();
                    })
                    .catch( (err) => {
                        console.log(err);
                        process.exit();
                    });
            });
    }
};

module.exports = (db, fs) => {
    return new OpenProjectParser(db, fs);
};
