const STORY_ID = 17;
const ISSUE_ID = 15;

class OpenProjectDB {

    constructor(knex){
        this.knex = knex;
    }

    getWorkPackages(project, version) {
	return this.knex.select('w.id as id',
                                'w.subject as subject',
                                'w.description as description',
                                'w.story_points as storyPoints',
                                's.name as status',
                                't.name as type')
	    .from('work_packages as w')
	    .leftJoin('statuses as s','s.id','w.status_id')
            .leftJoin('projects as p','p.id','w.project_id')
            .leftJoin('versions as v','v.id','w.fixed_version_id')
            .leftJoin('types as t', 't.id', 'w.type_id')
            .whereIn('type_id', [STORY_ID,ISSUE_ID])
            .andWhere({
		'p.name': project,
		'v.name': version
	    });
            
    };

    queryStatuses() {
        return this.knex.select().from('statuses').orderBy('is_closed');
    };

    queryProjects() {
        return this.knex.select('name').from('projects');
    };
        
    queryVersions(project) {
        return this.knex.select('v.name').from('versions as v')
            .leftJoin('projects as p', 'p.id', 'v.project_id')
            .where({'p.name': project});
    };

    queryUsers() {
        return this.knex.select('mail as name').from('users');
    };

    queryWiki(project, version) {
        return this.knex.select('wc.text', 'wc.id').from('wiki_contents as wc')
            .leftJoin('wiki_pages as wp', 'wp.id', 'wc.page_id')
            .leftJoin('wikis as w', 'w.id', 'wp.wiki_id')
            .leftJoin('projects as p', 'p.id', 'w.project_id')
            .where({'p.name': project, 'wp.title': version.replace(/\./g,'-') });
    }

    async insertWorkPackage(story) {
        let projectId = await this.knex.select('id').from('projects').where({name: story.project});
        let versionId = await this.knex.select('id').from('versions').where({name: story.version});
        let userId = await this.knex.select('id').from('users').where({mail: story.author});
        return this.knex('work_packages').insert({
            subject: story.subject,
            description: story.description,
            project_id: projectId[0].id,
            fixed_version_id: versionId[0].id,
            type_id: STORY_ID,
            status_id: story.statusId,
            priority_id: story.priority,
            author_id: userId[0].id,
            story_points: story.storyPoints
        });
    }
    
    updateWorkPackages(stories) {
        return stories.map( (story) => {
            if(story.id == 'null'){
                return this.insertWorkPackage(story).catch((err) => console.log(err));
            }
            else {
                return this.knex('work_packages')
                    .where({ id: story.id })
                    .update({
                        subject: story.subject,
                        description: story.description,
                        story_points: story.storyPoints,
                        status_id: story.statusId
                    })
                    .catch((err) => console.log(err));
            }
        });
    };

    updateWiki(wiki) {
        if(wiki.id == 'NONE') {
            return null;
        }
        return this.knex('wiki_contents')
                .update({ text: wiki.text })
                .where({ id: wiki.id })
                .catch((err) => console.log(err));
    };

    getMembers(project) {
        return this.knex.select('u.id','u.firstname','u.lastname','u.mail').from('members as m')
            .leftJoin('users as u', 'u.id','m.user_id')
            .leftJoin('member_roles as mr','m.id','mr.member_id')
            .leftJoin('roles as r','r.id','mr.role_id')
            .leftJoin('projects as p', 'p.id','m.project_id')
            .where({
                'p.name': project,
                'r.name': 'Developers'
            });
    }

    /**
     * If member is sent as null, the global performance is calculated instead of the member performance
     */
    async calculatePerformance(project, version, member){
        const totalStoryPointsWhereClause = {
            'p.name': project,
            'v.name': version,
        };
        if(member != null){
            totalStoryPointsWhereClause['assigned_to_id'] = member.id;
        }
        //Clone the object
        const doneStoryPointsWhereClause = JSON.parse(JSON.stringify(totalStoryPointsWhereClause));
        doneStoryPointsWhereClause.status_id = 2; //Done
        let query = this.knex.sum('story_points')
            .from('work_packages as w')
            .leftJoin('projects as p','p.id','w.project_id')
            .leftJoin('versions as v','v.id','w.fixed_version_id');
        let doneStoryPoints = await query.clone().whereIn('type_id',[STORY_ID,ISSUE_ID]).andWhere(doneStoryPointsWhereClause).first();
        let totalStoryPoints = await query.whereIn('type_id',[STORY_ID,ISSUE_ID]).andWhere(totalStoryPointsWhereClause).first();
        return {
            firstName: member != null ? member.firstname : undefined,
            lastName: member != null ? member.lastname : undefined,
            mail: member != null ? member.mail : undefined,
            total: totalStoryPoints['sum(`story_points`)'],
            done: doneStoryPoints['sum(`story_points`)']
        };        
    }
    
    async getMembersPerformance(project,version){
        let members = await this.getMembers(project,version);
        return Promise.all(members.map( (member) => {
            return this.calculatePerformance(project,version,member);
        }));
    }

    getGlobalPerformance(project, version){
        return this.calculatePerformance(project,version, null); 
    }
};

module.exports = (knex) => { return new OpenProjectDB(knex); };
