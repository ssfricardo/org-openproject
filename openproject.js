const fs = require('fs');
const options = {
    client: 'mysql',
    connection: {
        host : '',
        port : '',
        user : '',
        password : '',
        database : ''
    }
};
const knex = require('knex')(options);
const argv = require('minimist')(process.argv.slice(2));
const db = require('./openproject-db')( knex );
const serializer = require('./openproject-serializer')( db, fs );
const parser = require('./openproject-parser')( db, fs );

//Parse the arguments sent in the cli when running this script
switch (argv.action) {
case 'sprint':
    serializer.createOrgFile(argv.project, argv.version, argv.user);
    break;
case 'projects':
    serializer.getProjectsPiped();    
    break;
case 'versions':
    serializer.getVersionsPiped(argv.project);
    break;
case 'users':
    serializer.getUsersPiped();
    break;
case 'sync':
    parser.updateOpenProjectDatabase(argv.file);
    break;
default:
    console.log('Wrong parammeters!');
    process.exit();
    break;
};
