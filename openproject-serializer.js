/**
* Make a org-openproject file.
*/
class OpenProjectSerializer {

    constructor(db, fs){
        this.db = db;
        this.fs = fs;
    }

    writeWorkPackages(wps) {
	let body = '';
	wps.forEach( (wp) => {
	    const { id, subject, description, status, storyPoints, type } = wp;
	    body += `* ${this.replaceSpaces(status+'')} ${subject} :${type}:\n`;
	    body += ':PROPERTIES:\n';
	    body += `:work_package_id: ${id}\n`;
	    body += `:story_points: ${storyPoints}\n`;
	    body += `:type: ${type}\n`;
	    body += ':END:\n\n';
	    body += `${description} \n\n`;
	});
	return body;
    };

    replaceSpaces(str) {
        return str.replace(/ /g, '-');
    };

    writeWiki(wiki) {
        if(wiki[0] == null){
            return "\n* Wiki\n";
        }
        this.fs.writeFileSync(".wiki-tmp.textile", wiki[0].text);
        const execSync = require('child_process').execSync;
        this.code = execSync('pandoc --from textile --to org -o .wiki-tmp.org .wiki-tmp.textile');
        return this.fs.readFileSync('.wiki-tmp.org', 'utf8');       
    }
    
    writeHeaders(statuses, project, version, wiki, user) {
	let body = '';
	body += ':PROPERTIES: \n';
	body += '#+PROJECT: ' + project + '\n';
	body += '#+VERSION: ' + version + '\n';
        body += '#+WIKIPAGE: ' + (wiki[0] != null ? wiki[0].id : 'NONE') + '\n';
	body += '#+TODO: ';
        let isClosed = false;
	statuses.forEach( (status) => {
            if(!isClosed && status.is_closed){
                isClosed = true;
                body += ' | ';
            }
	    body += `${this.replaceSpaces(status.name+'')} `;
	});
        body += '\n#+SETUPFILE: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup\n'; //export to HTML
        body += '#+TITLE: Sprint-' + version + '\n'; 
        body += `#+AUTHOR: ${user}\n`;
	body += '\n:END: \n\n';
	return body; 
    };

    addPerformanceToWiki(wiki, performance, globalPerformance) {
        let wikiWithPerformance = wiki;
        //Remove the performance if exist
        if(wiki.includes('** Performance')){
            wikiWithPerformance = wiki.substring(0,wiki.indexOf('** Performance'));
        }
        let table = '** Performance \n\n';
        table += '*** Performance por persona. \n*Solo se puede calcular si todas las historias tienen asignado a un miembro*)\n\n|-\n';
        table += '|Nombre|Puntos Totales|Puntos Completados|Performance|\n|-\n';
        table += performance.map( (p) => {
            const per = '%' + (p.done / p.total * 100).toFixed(2);
            return `| ${p.firstName} ${p.lastName} | ${p.total} | ${p.done} | ${per} | \n`;
        }).join('');
        table += '|-\n\n';
        table += '*** Performance global del equipo.\n';
        table += `- Total Puntos: ${globalPerformance.total}\n`;
        table += `- Puntos terminados: ${globalPerformance.done}\n`;
        table += `- Performance equipo: %${(globalPerformance.done / globalPerformance.total * 100).toFixed(2)}\n`;
        return (wikiWithPerformance += table);
    }

    createOrgFile(project, version, user) {
        Promise.all([this.db.queryStatuses(),
                     this.db.getWorkPackages(project, version),
                     this.db.queryWiki(project,version),
                     this.db.getMembersPerformance(project,version),
                     this.db.getGlobalPerformance(project,version)])
            .then( ([statuses,wps, wiki, performance, globalPerformance]) => {                
                let response = this.writeHeaders(statuses, project, version, wiki, user);
                response += this.writeWorkPackages(wps);
                response += this.addPerformanceToWiki(this.writeWiki(wiki), performance, globalPerformance);
                console.log(response);
                process.exit();
        })
            .catch((err) => {
                console.log(err);
                process.exit();
            });
    };

    getProjectsPiped() {
        this.db.queryProjects().then((p) => {
            console.log(this.pipeNameResults(p));
            process.exit();
        });
    }

    getVersionsPiped(project) {
        this.db.queryVersions(project).then((v) => {
            console.log(this.pipeNameResults(v));
            process.exit();
        });
    }

    getUsersPiped() {
        this.db.queryUsers().then((u) => {
            console.log(this.pipeNameResults(u));
            process.exit();
        });
    }
    
    pipeNameResults(results){
        return results.map( (r) =>{
            return r.name;
        }).join('|');
    }
};

module.exports = (db, fs) => { return new OpenProjectSerializer(db, fs); };
