* org-openproject is a minor mode for emacs!
  
  This project is an integration between the open project database
  and an org-file. It was inspired in the org-trello project.

  *Important!* This is not by any means a full integration with OpenProject,
  this project was created with the only objective of acceleration the
  creation of SCRUM Sprints in my daly workflow. If you use a different
  workflow in Open Project maybe this emacs minor mode will be usless or
  insufficient. Said that...

** How does it work?
   
   Every 15 days I run a sprint planning session with my SCRUM teams, each team has
   an openproject project. In this sessions I perform this operations:
   

1. First go to OpenProject and create a new Version in each project 
   (This is not automated, I still need to go and create the Version)
   I make sure to name the version like Z.Y.X where Z is the major version of the project,
   Y is the Minor Version and X the Patch. Eg. 2.1.X. In the field wiki page name I
   put 2-1-X using the same name of the version but replacing the "." dots with "-".
   
2. Now I go to Wikis in the same project and create a new wiki with the same name as
   the one that I assigned in the version. (This is the last step that must be done
   in Open Project web interface, the rest will be done from Emacs in an org-mode file!)

3. Next I create an org-file in emacs. Eg. ~/Documents/myproject/sprints/2-1-X.org. The name
   is irrelevant but it can help you organize your work in yout local machine.

4. Finally, running the command C-u C-c o s the plugin will try to crawl all the information
   from openproject. The console should ask for the project and the version to crawl and
   add it as metadata to the org-file.

5. In the same way, you can update to open project running C-c o s. Any workpackage without ID
   will be created in Open Project in the correct project and in the correct version.

And that's it.

*A good practice is to always update from open project before editing your org file.*

** What are these mode keybindings?
   + `C-u C-c os`: Sync from openproject database into the file
   + `C-c os`: Sync the buffer into open project.

** What are the dependencies?

This works only with openproject 7.2.0 with a mysql database.

There's a REST APÏ well documented where I could integrate in a safer and maybe more manteinable way
but I started working in this project just as a hobby so I decided to hit the database directly. Maybe
in the future I will replace the db access for the open project API.

In the same way, I'm not proficient in elisp and I wanted a quick result so I wrote almost all the code
in javascript and the "el" only make calls to my node.js program. This could make a little inconvenient the
plugin but it works for my purposes. You need to have installed and in your path:

+ Node.js v11.14.0. This is where all the parsing, serializing and db access is done.
+ Pandoc For org to textile and textile to org wiki transformations.
