;;org-openproject minor mode

(setq node-script  "node ~/.emacs.d/lisp/org-openproject/openproject.js") 

(defun org-openproject-to-openproject ()
  "Sync open project database into the buffer."
  (interactive)
  (message (shell-command-to-string (concat node-script " --action=sync --file=" (buffer-file-name))))
  (org-openproject-from-openproject))

(defun org-openproject-from-openproject ()
  "Sync open project database into the buffer."
  (interactive)
  (setq project (org-global-prop-value "project"))
  (setq version (org-global-prop-value "version"))
  (setq author  (org-global-prop-value "author"))
  (if (not author)  (setq author  (get-users)))
  (if (not project) (setq project (get-projects)))
  (if (not version) (setq version (get-versions project)))
  (setq org-file (shell-command-to-string (concat node-script " --action=sprint --project=" project " --version=" version " --user=" author)))
  (erase-buffer)
  (insert org-file)
  (org-global-cycle))

(define-minor-mode org-openproject
  :lighter " opj"
  :keymap (let ((map (make-sparse-keymap)))
	    (define-key map (kbd "C-c os") 'org-openproject-to-openproject)
	    (define-key map (kbd "C-u C-c os") 'org-openproject-from-openproject)
	    map))

(provide 'org-openproject)

(defun get-projects ()
  (completing-read
   "Select a project:"
   (split-string (shell-command-to-string (concat node-script " --action=projects")) "|")
   nil t ""))

(defun get-versions (project)
  (completing-read
   "Select a version:"
   (split-string (shell-command-to-string (concat node-script " --action=versions --project=" project)) "|")
   nil t ""))

(defun get-users ()
  (completing-read
   "Select a user:"
   (split-string (shell-command-to-string (concat node-script " --action=users")) "|")
   nil t ""))

(defun org-global-prop-value (key)
  "Get global org property KEY of current buffer."
  (org-element-property :value (car (org-global-props key))))

(defun org-global-props (&optional property buffer)
  "Get the plists of global org properties of current buffer."
  (unless property (setq property "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'keyword (lambda (el) (when (string-match property (org-element-property :key el)) el)))))

(defun org-openproject-insert-story ()
  "Insert a new story"
  (interactive
   (progn
     (beginning-of-line) 
     (insert (format "* Nuevo My New Story :Story:\n"))
     (insert (format ":PROPERTIES:\n"))
     (insert (format ":work_package_id: null\n"))
     (insert (format ":story_points: 0\n"))
     (insert (format ":type: story\n"))
     (insert (format ":END:\n\n"))
     (insert (format "Description\n\n"))
     (previous-line 1))))

